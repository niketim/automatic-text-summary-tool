#2012B5A7848P-Saurabh Kumar
#TextRank Algorithm

import io
import nltk,re,pprint
import itertools
import math
from graph import graph
from nltk.corpus import stopwords

#finds similarity between two sentances to appropriately assign weight to their link.
#Greater the similarity, more will be the weight
def similarity(first,second):
	count =0
	#remove stopwords 
	stop=stopwords.words('english')
	first=list(set(first)-set(stop))
	second=list(set(second)-set(stop))
	
	#counts common words in both sentences
	for word1 in first:
		for word2 in second:
			if(word1==word2):
				count+=1
				break;
	if(count == 0):
		return 0
	else:
		#returns a measure of similarity between the sentences
		return (count/(math.log(len(first),2)+math.log(len(second),2)))


#represents text as graph	
def buildGraph(nodes):
	gr=graph(nodes)
	#nodePairs contains all the combinations of the sentances
	nodePairs = list(itertools.combinations(nodes, 2))

	#add edges to the graph (weighted by similarity between them)
	for pair in nodePairs:
		firstString = pair[0]
		secondString = pair[1]
		#similarity is the measure of the weight. More is the similarity, more will be the weight.
		edgeWeight = similarity(nltk.word_tokenize(firstString), nltk.word_tokenize(secondString))
		if(edgeWeight!=0):
			gr.addEdge(firstString, secondString, edgeWeight)
	return gr
	
#helper function for calculating sentance rank	
def weightSum(nodes):
	i=0
	s=0;
	while(i<len(nodes)):
		s=s+nodes[i]
		i=i+1
	return s

#helper function for calculating sentance rank	
def neighbourImportance(graph,nodes,ranks):
	i=0;
	importance=0.0
	while(i<len(nodes)):
		importance=importance+(ranks[i][1]*nodes[i]/weightSum(graph.getNeighbours(i)))
		i=i+1
		
	return importance


#function to rank sentances in decreasing order of importance    
def textRank(graph,alpha=0.85):
	if len(graph.nodeContent) == 0:
		return {}
	else:
		#initially every sentence is given equal rank i.e. 100. It could be any number to start with
		temp=100.0
		
		nodeRanks = [[temp for i in range(3)] for j in range(len(graph.nodeContent))]
		tempRanks = [100.0] * len(graph.nodeContent)
		
		i=0
		
		while(i<len(tempRanks)):
			nodeRanks[i][0]=graph.getNode(i)
			nodeRanks[i][2]=i+1
			i=i+1
		
		#flag to indicate convergence	
		hasConverged=0
		while(hasConverged==0):
			hasConverged=1
			i=0;
			while(i<len(tempRanks)):
				#calculates new rank for each sentence
				tempRanks[i]=(1-alpha)+alpha*neighbourImportance(graph,graph.getNeighbours(i),nodeRanks)
				if(math.fabs(tempRanks[i]-nodeRanks[i][1])>0.000001):
					hasConverged=0
				i=i+1	
					
			i=0
			while(i<len(tempRanks)):
				nodeRanks[i][1]=tempRanks[i]
				i=i+1
				
	
	return nodeRanks


#summarize the entire text
def summarize(text):
	#extract individual sentances
	sentanceNode = nltk.sent_tokenize(text)
	#represent sentances as nodes
	Graph = buildGraph(sentanceNode)
	#gets relative rank of each sentence
	ranks=textRank(Graph)	
	
	ranks.sort(key=lambda x: (x[1] * -1, x[2]))
	
    #return a 5 sentances ~100 words summary

	if(len(ranks)<5):
		j=len(ranks)
	else:
		j=5
		
	ranks=ranks[0:j]
	ranks.sort(key=lambda x: (x[2], x[1]))
	
	summary=ranks[0][0]
	i=1
	while(i<j):
		summary=summary+' '+ranks[i][0]
		i=i+1
	
	
	
	return summary

#Support - inspired by stackoverflow.com
#writes summary of text to appropriate file
def writeFiles(summary, fileName):
    print "Generating output to " + 'summaries/' + fileName
    summaryFile = io.open('summaries/' + fileName, 'w')
    summaryFile.write(summary)
    summaryFile.close()

    print "-"
