With less than 48 hours separating the G20 Summit from the Paris carnage, the discourse at the meeting of leaders from the world’s 20 major economies was dominated by condemnation of the terrorist attacks.

World leaders gathered at Antalya’s picturesque Belek area — including Prime Minister Narendra Modi, US and Russian Presidents Barack Obama and Vladimir Putin, and President Recep Tayyip Erdogan of Turkey, the host of the Summit — issued strong statements against terrorism and the attacks.

Modi called for cutting off funding to terror groups, Obama for redoubling of efforts to eliminate ISIS, Erdogan for a combined fight against international terrorism, and Putin for unity in global efforts against terror.

“We are meeting in the tragic shadow of dreadful acts of terrorism, united by a sense of shock, pain and outrage,” Modi said. “We are united in condemning the barbaric attacks in Paris this week, and the recent bombings in Ankara and Lebanon. We share the sorrow of Russia for the lost lives in the fallen aircraft in Sinai.

“These alone are a stark reminder of the dark force we face — larger than specific groups and particular targets and territories. It is a major global challenge of our times. It not only takes a tragic toll of lives, it also extracts a huge economic cost and threatens our way of life... combating it must be a major priority for G20.”

On October 31, a Russian passenger airliner crashed in Egypt’s Sinai peninsula, possibly after a bomb exploded on board, killing 224 passengers and crew.

On November 12, at least 41 people were killed in two bomb attacks by the Islamic State in the Lebanese capital Beirut. On October 10, 107 people were killed in Ankara in the deadliest terrorist attack in the history of modern Turkey.

G20, the PM said, should “promote stronger global action to address security challenges, including through a comprehensive global strategy to put an end to finance, supplies and communication channels of the terrorists, stopping the flow of arms and explosives to terrorist groups, creating special international legal regime to disrupt terrorist activities, cooperation in preventing the use of cyber networks by terrorist groups, and early adoption of Comprehensive Convention on International Terrorism”.

At a meeting with leaders of other BRICS nations on the sidelines of the Summit, Modi said: “We stand united in strongly condemning the dreadful acts of terrorism in Paris... The entire humanity must stand together as one against terrorism. The need for a united global effort to combat terrorism has never been more urgent.”

Combating terrorism must be priority for BRICS nations, the Prime Minister said. He expressed the “deepest sympathy and support” for Russia for the Sinai incident, and also mentioned Ankara and Beirut.

Obama, who met with Erdogan, said the G20 meets to discuss the world’s important economic issues, “but (this time) skies have been darkened by the horrific attacks in Paris”. He spoke of the attacks in Ankara, and the “twisted ideology” behind them.

These attacks, Obama said, were not just against France and Turkey, but against the civilized world. “We stand in solidarity in hunting down the perpetrators of the attacks and bring them to justice,” Obama said, adding the US would “redouble efforts” to “eliminate” the terrorist group.

Erdogan said the agenda of the summit was “very different” following the Paris attacks. “We need to lead an international fight within a coalition against collective acts of terrorism,” Erdogan said. “We have all been shocked.” He added, “I believe that our stance against international terrorism will find its expression in a very strong, tough message at the G20 Summit.”

Sources told The Indian Express that officials were discussing the possibility of issuing either a separate statement on terrorism, or introducing a strong paragraph in the G20 communique in the wake of the attacks.

President Putin said the world would be able to deal with the terrorist threat only if the international community united in the effort. “We have all seen the horror that took place recently in Paris and we sympathise with the affected people. Russia is always in favour of joining efforts to deal effectively with the terrorist threat,” he said. “Of course, it is necessary to act in strict accordance with the United Nations,” he added.

Chinese President Xi Jinping said, “We will work more closely with the international community to reject and fight terrorism in all its manifestations.”

European Council President Donald Tusk said, “It should be our aim to coordinate our action against Daesh. Co-operation between us and Russia is a crucial one.”