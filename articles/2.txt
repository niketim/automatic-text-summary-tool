Tamil Nadu continued to experience monsoon fury on Sunday, with heavy rains pounding various parts of the state under the influence of a well marked low pressure area over Bay of Bengal, as the death toll from rain-related incidents climbed to 59.

There seemed to be no respite from the downpour with many parts of the city coming under water even as the weatherman forecast more rains for the next 24 hours, beginning 08:30 am.

The India Meteorological Department said in a bulletin on Sunday that the well-marked low pressure area over southwest Bay of Bengal adjoining Sri Lanka persisted and "it is likely to move west-northwestwards towards Tamil Nadu coast and would concentrate into a Depression during next 24 hours." Under its influence, more rains were expected in the next 24 hours, the Regional Meteorological Department said.

Anaikaracharthiram (Nagapattinam) received the maximum rainfall of 18 cm recorded till 8:30 am, RMC Director SR Ramanan said, adding, Sirkali from the same district registered 17 cm. Chennai received 3 cm rainfall between 8:30 am and 11:30 am on Sunday. 

He said heavy to very heavy rains could be expected in the northern coastal districts of the state in the next 24 hours while there could be rain in the rest of the districts. Rains were also expected in Puducherry on Monday. The seas would be rough, he said, warning fishermen against venturing for fishing.

Meanwhile, four persons died due to various rain-related incidents on 13 and 14 November, the government said.

Chief Minister Jayalalithaa condoled the death of the four persons, three of whom died due to drowning in Kancheepuram district while one person in Vellore was killed in wall collapse. She announced a sum of Rs 4 lakh each to the families of the victims from the Disaster Relief Fund.

The incessant rains severely crippled normal life in the state capital Chennai, where most roads, residential areas and low-lying parts were inundated.

Subways at suburban Chennai connecting the residential areas were inundated, rendering them useless for commutation.

The sparse Sunday crowd of motorists were seen discussing alternative routes to reach their respective destinations.

Water-clogged roads resulted in slow movement of vehicles even as pedestrians were seen wading through waist-deep to knee-deep water in many places. Many residents were forced to stay put inside their homes following the inclement weather. Trains on the suburban Chennai Egmore-Tambaram were running slow.

The inclement weather also affected flight services. A Colombo-Chennai Sri Lankan flight was diverted back to that city while a city-bound IndiGo aircraft from Delhi was diverted to Bengaluru, airport officials said. A Silk Air flight from Singapore to Chennai was also diverted to Bengaluru, they said, adding that many services were delayed by between 30-45 minutes.

Meanwhile, the government announced closure of schools and colleges in Chennai on Monday. Educational institutions in several coastal districts including Kancheepuram, Tiruvallur and Tiruchirappalli, and Vellore in northern part of the states would remain closed on Monday in view of the rains, officials said.

In Coimbatore, BJP's state unit president Tamilisai Soundararajan said the party would submit a detailed report on the situation in the rain-battered Cuddalore district to Prime Minister Narendra Modi.