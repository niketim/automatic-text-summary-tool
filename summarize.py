#2012B5A7848P-Saurabh Kumar
#TextRank Algorithm


import io
from operator import itemgetter
import os
from textrank import *
import sys



try:
	#reads the text
	articles = os.listdir("articles")
	for article in articles:
		print 'Reading articles/' + article
		articleFile = io.open('articles/' + article, 'r')
		text = articleFile.read()
		summary = summarize(text)	
		writeFiles(summary, article)
except:
	error=sys.exc_info()[0]
	print(error)
